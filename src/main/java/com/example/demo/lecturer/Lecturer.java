package com.example.demo.lecturer;

import com.example.demo.course.Course;
import com.example.demo.lesson.Lesson;
import com.example.demo.user.User;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "lecturer")
@Getter
@Setter
@NoArgsConstructor
public class Lecturer {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Integer age;

    @OneToMany
    @JoinColumn(name = "fk_lecturer_id")
    @BatchSize(size = 150)
    @JsonIgnore
    private Set<Lesson> lessons = new HashSet<>();

    @OneToMany
    @JoinColumn(name = "fk_mentor_id")
    @BatchSize(size = 150)
    @JsonIgnore
    private Set<Course> mentoredCourses = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "lecturer", cascade = CascadeType.ALL)
    @JsonIgnore
    private User user;

    // for create
    public Lecturer(LecturerDto lecturerDto) {
        this.name = lecturerDto.getName();
        this.age = lecturerDto.getAge();
    }

    //for tests
    public Lecturer(Long id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lecturer lecturer = (Lecturer) o;
        return id.equals(lecturer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }


    public LecturerDto toDto() {
        return new LecturerDto(this.getId(), this.getName(), this.getAge(), this.getUser().getPassword());
    }
}
