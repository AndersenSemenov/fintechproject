package com.example.demo.lecturer;

import com.example.demo.course.Course;
import com.example.demo.lesson.Lesson;
import lombok.*;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import java.util.Objects;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LecturerDto {

    private Long id;

    @NotBlank(message = "Name can't be empty")
    private String name;

    @Range(min = 0, message = "Age can't be less than 0")
    private Integer age;

    @NotBlank(message = "Password can't be empty")
    private String password;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LecturerDto that = (LecturerDto) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(age, that.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age);
    }
}
