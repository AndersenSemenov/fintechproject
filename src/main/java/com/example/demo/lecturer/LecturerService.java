package com.example.demo.lecturer;

import com.example.demo.course.Course;
import com.example.demo.lesson.Lesson;
import com.example.demo.role.Role;
import com.example.demo.user.User;
import com.example.demo.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.HashSet;

@Service
@AllArgsConstructor
public class LecturerService {

    private final LecturerRepository lecturerRepository;
    private final UserService userService;
    private final PasswordEncoder encoder;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Iterable<Lecturer> getAll() {
        return lecturerRepository.findAll();
    }

    @PostAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or returnObject.name == authentication.principal.username)")
    public Lecturer getById(Long id) {
        return lecturerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No lecturer was found with id = " + id));
    }

    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or this.getById(#id).name == authentication.principal.username)")
    public Iterable<Lesson> getSchedule(Long id) {
        return getById(id).getLessons();
    }

    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or this.getById(#id).name == authentication.principal.username)")
    public Iterable<Course> getMentoredCourse(Long id) {
        return getById(id).getMentoredCourses();
    }

    public Lecturer create(LecturerDto lecturerDto) {
        if (userService.existsByName(lecturerDto.getName())) {
            throw new IllegalArgumentException("User with such login already exists");
        }

        Lecturer lecturer = new Lecturer(lecturerDto);

        User user = new User();
        user.setName(lecturer.getName());
        user.setPassword(encoder.encode(lecturerDto.getPassword()));
        user.setRole(new Role(2L, "ROLE_TEACHER"));
        lecturer.setUser(user);
        user.setLecturer(lecturer);

        return lecturerRepository.save(lecturer);
    }

    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or this.getById(#lecturerDto.id).name == authentication.principal.username)")
    public Lecturer update(LecturerDto lecturerDto) {
        Lecturer lecturer = getById(lecturerDto.getId());

        lecturer.setAge(lecturerDto.getAge());
        lecturer.setName(lecturerDto.getName());

        User user = lecturer.getUser();
        user.setName(lecturerDto.getName());
        user.setPassword(encoder.encode(lecturerDto.getPassword()));

        return lecturerRepository.save(lecturer);
    }

    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or this.getById(#id).name == authentication.principal.username)")
    public void deleteById(Long id) {
        lecturerRepository.deleteById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteAll() {
        lecturerRepository.deleteAll();
    }

    @Transactional
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or this.getById(#lecturerId).name == authentication.principal.username)")
    public void leaveCourseMentoring(Long lecturerId, Long courseId) {
        lecturerRepository.leaveCourseMentoring(lecturerId, courseId);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void getCourseMentoring(Long lecturerId, Long courseId) {
        lecturerRepository.getCourseMentoring(lecturerId, courseId);
    }

    @Transactional
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or this.getById(#lecturerId).name == authentication.principal.username)")
    public void leaveLesson(Long lecturerId, Long lessonId) {
        lecturerRepository.leaveLesson(lecturerId, lessonId);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void getLesson(Long lecturerId, Long lessonId) {
        lecturerRepository.getLesson(lecturerId, lessonId);
    }
}
