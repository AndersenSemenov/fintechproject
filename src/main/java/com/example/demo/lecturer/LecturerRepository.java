package com.example.demo.lecturer;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface LecturerRepository extends CrudRepository<Lecturer, Long> {

    @Modifying
    @Query(value = """
            update
            course set
                   fk_mentor_id = null
             where course.fk_mentor_id = :lecturerId and course.id = :courseId
            """,
            nativeQuery = true)
    public void leaveCourseMentoring(@Param("lecturerId") Long lecturerId, @Param("courseId") Long courseId);

    @Modifying
    @Query(value = """
            update
            course set
                   fk_mentor_id = :lecturerId
             where course.id = :courseId
            """,
            nativeQuery = true)
    public void getCourseMentoring(@Param("lecturerId") Long lecturerId, @Param("courseId") Long courseId);

    @Modifying
    @Query(value = """
            update
            lesson set
                   fk_lecturer_id = null
             where lesson.fk_lecturer_id = :lecturerId and lesson.id = :lessonId
            """,
            nativeQuery = true)
    public void leaveLesson(@Param("lecturerId") Long lecturerId, @Param("lessonId") Long lessonId);

    @Modifying
    @Query(value = """
            update
            lesson set
                   fk_lecturer_id = :lecturerId
             where lesson.id = :lessonId
            """,
            nativeQuery = true)
    public void getLesson(@Param("lecturerId") Long lecturerId, @Param("lessonId") Long lessonId);
}
