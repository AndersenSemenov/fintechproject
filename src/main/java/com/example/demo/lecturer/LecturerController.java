package com.example.demo.lecturer;

import com.example.demo.course.Course;
import com.example.demo.lesson.Lesson;
import lombok.AllArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.ValidationException;

@RestController
@RequestMapping("/lecturer")
@AllArgsConstructor
public class LecturerController {
    private final LecturerService lecturerService;

    @GetMapping
    public Iterable<Lecturer> getAll() {
        return lecturerService.getAll();
    }

    @GetMapping("/{id}")
    public Lecturer getById(@PathVariable("id") Long id) {
        return lecturerService.getById(id);
    }

    @GetMapping("/schedule/{id}")
    public Iterable<Lesson> getSchedule(@PathVariable("id") Long id) {
        return lecturerService.getSchedule(id);
    }

    @GetMapping("/mentoredCourse/{id}")
    public Iterable<Course> getMentoredCourse(@PathVariable("id") Long id) {
        return lecturerService.getMentoredCourse(id);
    }

    @PostMapping
    public Lecturer create(@Valid @RequestBody LecturerDto lecturerDto) {
        return lecturerService.create(lecturerDto);
    }

    @PutMapping
    public void update(@Valid @RequestBody LecturerDto lecturerDto) {
        lecturerService.update(lecturerDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        lecturerService.deleteById(id);
    }

    @DeleteMapping
    public void deleteAll() {
        lecturerService.deleteAll();
    }

    @PutMapping("/{lecturerId}/stopMentoring/{courseId}")
    public void leaveCourseMentoring(@PathVariable("lecturerId") Long lecturerId, @PathVariable("courseId") Long courseId) {
        lecturerService.leaveCourseMentoring(lecturerId, courseId);
    }

    @PutMapping("/{lecturerId}/getMentoring/{courseId}")
    public void getCourseMentoring(@PathVariable("lecturerId") Long lecturerId, @PathVariable("courseId") Long courseId) {
        lecturerService.getCourseMentoring(lecturerId, courseId);
    }

    @PutMapping("/{lecturerId}/leaveLesson/{lessonId}")
    public void leaveLesson(@PathVariable("lecturerId") Long lecturerId, @PathVariable("lessonId") Long lessonId) {
        lecturerService.leaveLesson(lecturerId, lessonId);
    }

    @PutMapping("/{lecturerId}/getLesson/{lessonId}")
    public void getLesson(@PathVariable("lecturerId") Long lecturerId, @PathVariable("lessonId") Long lessonId) {
        lecturerService.getLesson(lecturerId, lessonId);
    }

}
