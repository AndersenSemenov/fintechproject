package com.example.demo;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class FintechCourseWorkApplication {

	@Autowired
	Flyway flyway;

	@Component
	public class rest implements ApplicationRunner {

		@Override
		public void run(ApplicationArguments args) throws Exception {
			flyway.clean();
			flyway.migrate();
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(FintechCourseWorkApplication.class, args);
	}
}
