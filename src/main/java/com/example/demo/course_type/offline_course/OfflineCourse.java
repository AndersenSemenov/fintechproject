package com.example.demo.course_type.offline_course;

import com.example.demo.course_type.abstract_course_type.CourseType;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity
@Table(name = "offline_course")
@PrimaryKeyJoinColumn(name = "id")
public class OfflineCourse extends CourseType {

    @Column(name = "university_name")
    private String universityName;

    @NotBlank(message = "Address of course lessons can't be empty")
    @Column(name = "address")
    private String address;

    public OfflineCourse() {}

    public OfflineCourse(Long id, String universityName, String address) {
        super(id);
        this.universityName = universityName;
        this.address = address;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OfflineCourse that = (OfflineCourse) o;
        return Objects.equals(super.getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getId());
    }
}
