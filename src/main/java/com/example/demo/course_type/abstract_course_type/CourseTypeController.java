package com.example.demo.course_type.abstract_course_type;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("typecourse")
@AllArgsConstructor
public class CourseTypeController {

    private final CourseTypeService courseTypeService;

    @GetMapping("/{id}")
    public CourseType getById(@PathVariable("id") Long id) {
        return courseTypeService.getById(id);
    }

    @PostMapping
    public CourseType create(@Valid @RequestBody CourseType courseType) {
        return courseTypeService.create(courseType);
    }
}
