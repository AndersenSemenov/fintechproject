package com.example.demo.course_type.abstract_course_type;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CourseTypeRepository extends CrudRepository<CourseType, Long> {
}
