package com.example.demo.course_type.abstract_course_type;

import com.example.demo.course_type.offline_course.OfflineCourse;
import com.example.demo.course_type.offline_course.OfflineCourseRepository;
import com.example.demo.course_type.online_course.OnlineCourseRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@AllArgsConstructor
public class CourseTypeService {

    private final OfflineCourseRepository offlineRepo;
    private final OnlineCourseRepository onlineRepo;

    public CourseType getById(Long id) {
        return offlineRepo.existsById(id) ?
                offlineRepo.findById(id).get() :
                onlineRepo.findById(id)
                        .orElseThrow(() -> new EntityNotFoundException("No course type was found with id = " + id));
    }

    public CourseType create(CourseType courseType) {
        return courseType instanceof OfflineCourse ?
                offlineRepo.save(courseType) :
                onlineRepo.save(courseType);
    }
}
