package com.example.demo.course_type.abstract_course_type;

import com.example.demo.course_type.offline_course.OfflineCourse;
import com.example.demo.course_type.online_course.OnlineCourse;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase;

public class CourseTypeIdResolver extends TypeIdResolverBase {

    private JavaType superType;

    @Override
    public void init(JavaType baseType) {
        superType = baseType;
    }

    @Override
    public JsonTypeInfo.Id getMechanism() {
        return JsonTypeInfo.Id.NAME;
    }

    @Override
    public String idFromValue(Object o) {
        return idFromValueAndType(o, o.getClass());
    }

    @Override
    public String idFromValueAndType(Object o, Class<?> subType) {
        String typeId = switch (subType.getSimpleName()) {
            case "OfflineCourse" -> "offline";
            case "OnlineCourse" -> "online";
            default -> null;
        };
        return typeId;
    }

    @Override
    public JavaType typeFromId(DatabindContext context, String id) {
        Class<?> subType = switch (id) {
            case "offline" -> OfflineCourse.class;
            case "online" -> OnlineCourse.class;
            default -> null;
        };
        if (subType == null) {
            throw new IllegalArgumentException("No course type with type " + id);
        }
        return context.constructSpecializedType(superType, subType);
    }
}