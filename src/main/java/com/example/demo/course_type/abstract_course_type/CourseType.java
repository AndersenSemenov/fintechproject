package com.example.demo.course_type.abstract_course_type;

import com.example.demo.course.Course;
import com.example.demo.course_type.offline_course.OfflineCourse;
import com.example.demo.course_type.online_course.OnlineCourse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "course_type")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include= JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeIdResolver(value = CourseTypeIdResolver.class)
@JsonSubTypes({
        @JsonSubTypes.Type(value = OfflineCourse.class, name = "offline"),
        @JsonSubTypes.Type(value = OnlineCourse.class, name = "online")
})
public abstract class CourseType {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_course_type")
    @JsonIgnore
    private Set<Course> courses;

    public CourseType() {}

    public CourseType(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }
}
