package com.example.demo.course_type.online_course;

import com.example.demo.course_type.abstract_course_type.CourseType;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity
@Table(name = "online_course")
@PrimaryKeyJoinColumn(name = "id")
public class OnlineCourse extends CourseType {

    @Column(name = "url")
    @NotBlank(message = "url for course can't be empty")
    private String url;

    public OnlineCourse() {}

    public OnlineCourse(Long id, String url) {
        super(id);
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OnlineCourse that = (OnlineCourse) o;
        return Objects.equals(super.getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getId());
    }
}
