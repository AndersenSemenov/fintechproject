package com.example.demo.course_type.online_course;

import com.example.demo.course_type.abstract_course_type.CourseTypeRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OnlineCourseRepository extends CourseTypeRepository {
}
