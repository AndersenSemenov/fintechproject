package com.example.demo.category.humanitarian_category;

import com.example.demo.category.abstract_category.Category;
import com.example.demo.course.Course;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "humanitarian_category")
@PrimaryKeyJoinColumn(name = "id")
@JsonTypeName(value = "humanitarian")
public class HumanitarianCategory extends Category {

    @Min(value = 1, message = "Number of debates should be at least 1")
    @Column(name = "number_of_debates")
    private Integer numberOfDebates;

    public HumanitarianCategory() {}

    public HumanitarianCategory(Long id, Integer numberOfDebates) {
        super(id);
        this.numberOfDebates = numberOfDebates;
    }

    public Integer getNumberOfDebates() {
        return numberOfDebates;
    }

    public void setNumberOfDebates(Integer numberOfDebates) {
        this.numberOfDebates = numberOfDebates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HumanitarianCategory that = (HumanitarianCategory) o;
        return Objects.equals(super.getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getId());
    }
}
