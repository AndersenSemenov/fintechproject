package com.example.demo.category.humanitarian_category;

import com.example.demo.category.abstract_category.CategoryRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HumanitarianRepository extends CategoryRepository {
}
