package com.example.demo.category.technical_category;

import com.example.demo.category.abstract_category.CategoryRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TechnicalRepository extends CategoryRepository  {
}
