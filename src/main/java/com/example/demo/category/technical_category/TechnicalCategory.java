package com.example.demo.category.technical_category;


import com.example.demo.category.abstract_category.Category;
import com.example.demo.course.Course;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "technical_category")
@PrimaryKeyJoinColumn(name = "id")
@JsonTypeName(value = "technical")
public class TechnicalCategory extends Category {

    @NotBlank(message = "Language name can't be empty")
    @Column(name = "language")
    private String language;

    public TechnicalCategory() {}

    public TechnicalCategory(Long id, String language) {
        super(id);
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TechnicalCategory that = (TechnicalCategory) o;
        return Objects.equals(super.getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getId());
    }
}
