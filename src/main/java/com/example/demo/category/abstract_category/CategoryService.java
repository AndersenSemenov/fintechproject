package com.example.demo.category.abstract_category;

import com.example.demo.category.humanitarian_category.HumanitarianCategory;
import com.example.demo.category.humanitarian_category.HumanitarianRepository;
import com.example.demo.category.technical_category.TechnicalRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@AllArgsConstructor
public class CategoryService {

    private final HumanitarianRepository humRepo;
    private final TechnicalRepository techRepo;

    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public Category getById(Long id) {
        return humRepo.existsById(id) ?
                humRepo.findById(id).get() :
                techRepo.findById(id)
                        .orElseThrow(() -> new EntityNotFoundException("No category was found with id = " + id));
    }

    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public Category create(Category category) {
        return category instanceof HumanitarianCategory ?
                humRepo.save(category) :
                techRepo.save(category);
    }
}
