package com.example.demo.category.abstract_category;

import com.example.demo.category.humanitarian_category.HumanitarianCategory;
import com.example.demo.category.technical_category.TechnicalCategory;
import com.example.demo.course.Course;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "category")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include= JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeIdResolver(value = CategoryIdResolver.class)
@JsonSubTypes({
        @JsonSubTypes.Type(value = TechnicalCategory.class, name = "technical"),
        @JsonSubTypes.Type(value = HumanitarianCategory.class, name = "humanitarian")
})
public abstract class Category {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_category_id")
    @JsonIgnore
    private Set<Course> courses;

    public Category() {}

    public Category(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }
}
