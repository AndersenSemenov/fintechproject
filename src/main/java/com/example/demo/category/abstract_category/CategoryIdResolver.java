package com.example.demo.category.abstract_category;

import com.example.demo.category.humanitarian_category.HumanitarianCategory;
import com.example.demo.category.technical_category.TechnicalCategory;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase;

public class CategoryIdResolver extends TypeIdResolverBase {

    private JavaType superType;

    @Override
    public void init(JavaType baseType) {
        superType = baseType;
    }

    @Override
    public JsonTypeInfo.Id getMechanism() {
        return JsonTypeInfo.Id.NAME;
    }

    @Override
    public String idFromValue(Object o) {
        return idFromValueAndType(o, o.getClass());
    }

    @Override
    public String idFromValueAndType(Object o, Class<?> subType) {
        String typeId = switch (subType.getSimpleName()) {
            case "TechnicalCategory" -> "technical";
            case "HumanitarianCategory" -> "humanitarian";
            default -> null;
        };
        return typeId;
    }

    @Override
    public JavaType typeFromId(DatabindContext context, String id) {
        Class<?> subType = switch (id) {
            case "technical" -> TechnicalCategory.class;
            case "humanitarian" -> HumanitarianCategory.class;
            default -> null;
        };
        if (subType == null) {
            throw new IllegalArgumentException("No category with type " + id);
        }
        return context.constructSpecializedType(superType, subType);
    }
}
