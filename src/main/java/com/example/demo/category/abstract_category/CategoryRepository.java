package com.example.demo.category.abstract_category;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CategoryRepository extends CrudRepository<Category, Long> {
}
