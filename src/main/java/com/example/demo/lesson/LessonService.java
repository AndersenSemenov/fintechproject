package com.example.demo.lesson;

import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@AllArgsConstructor
public class LessonService {

    private final LessonRepository lessonRepository;

    @PostAuthorize("""
            isAuthenticated() and
            (hasRole('ROLE_ADMIN') or
            returnObject.lecturer.name == authentication.principal.username or
            returnObject.course.mentor.name == authentication.principal.username)
            """)
    public Lesson getById(Long id) {
        return lessonRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No lesson was found with id = " + id));
    }

    @PostAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or returnObject.course.mentor.name == authentication.principal.username)")
    public Lesson update(LessonDto lessonDto) {
        Lesson lesson = getById(lessonDto.getId());
        lesson.setName(lessonDto.getName());
        lesson.setDescription(lessonDto.getDescription());
        lesson.setDate(lessonDto.getDate());
        return lessonRepository.save(lesson);
    }

    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or this.getById(#id).course.mentor.name == authentication.principal.username)")
    public void deleteById(Long id) {
        lessonRepository.deleteById(id);
    }
}
