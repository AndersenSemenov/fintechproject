package com.example.demo.lesson;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LessonDto {

    private Long id;

    @NotBlank(message = "Lesson name can't be empty")
    private String name;

    private String description;

    private Date date;
}
