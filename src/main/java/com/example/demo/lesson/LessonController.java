package com.example.demo.lesson;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/lesson")
@AllArgsConstructor
public class LessonController {

    private final LessonService lessonService;

    @GetMapping("/{id}")
    public Lesson getById(@PathVariable("id") Long id) {
        return lessonService.getById(id);
    }

    @PutMapping
    public Lesson update(@Valid @RequestBody LessonDto lessonDto) {
        return lessonService.update(lessonDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        lessonService.deleteById(id);
    }
}
