package com.example.demo.course;

import com.example.demo.category.abstract_category.Category;
import com.example.demo.course_type.abstract_course_type.CourseType;
import com.example.demo.lecturer.Lecturer;
import com.example.demo.lesson.Lesson;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "course")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Course {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "number_of_students")
    private Integer numberOfStudents;

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    @BatchSize(size = 100)
    @JsonIgnore
    private Set<Lesson> lessons = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "fk_mentor_id")
    @JsonIgnore
    private Lecturer mentor;

    @ManyToOne
    @JoinColumn(name = "fk_category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "fk_course_type")
    private CourseType courseType;

    //for create
    public Course(CourseDto courseDto) {
        this.name = courseDto.getName();
        this.description = courseDto.getDescription();
        this.numberOfStudents = courseDto.getNumberOfStudents();
        this.category = courseDto.getCategory();
        this.courseType = courseDto.getCourseType();
    }

    //for createByCoping
    public Course(Lecturer mentor, Course course) {
        this.name = course.getName();
        this.description = course.getDescription();
        this.numberOfStudents = course.getNumberOfStudents();
        this.mentor = mentor;
        this.lessons = course.getLessons();
        this.category = course.getCategory();
        this.courseType = course.getCourseType();
    }

    public CourseDto toDto() {
        return new CourseDto(this.getId(), this.getName(), this.getDescription(), this.getNumberOfStudents(), this.getCategory(), this.getCourseType());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(id, course.id) && Objects.equals(name, course.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}