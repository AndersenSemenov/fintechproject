package com.example.demo.course;

import com.example.demo.category.abstract_category.Category;
import com.example.demo.course_type.abstract_course_type.CourseType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CourseDto {

    private Long id;

    @NotBlank(message = "Course name can't be empty")
    private String name;

    private String description;

    @Range(min = 0, message = "Number of students can't be less than 0")
    private Integer numberOfStudents;

    private Category category;

    private CourseType courseType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseDto courseDto = (CourseDto) o;
        return Objects.equals(id, courseDto.id) && Objects.equals(name, courseDto.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
