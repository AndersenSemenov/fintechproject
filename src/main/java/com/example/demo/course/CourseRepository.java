package com.example.demo.course;

import com.example.demo.lesson.LessonDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface CourseRepository extends CrudRepository<Course, Long> {

    @Query(value = """
            insert into lesson(name, description, fk_course_id, lesson_date)
            values (:#{#lesson_dto.name}, :#{#lesson_dto.description}, :courseId, :#{#lesson_dto.date})
            returning *;
            """,
            nativeQuery = true)
    public Long addLessonToCourse(@Param("courseId") Long courseId, @Param("lesson_dto") LessonDto lessonDto);
}
