package com.example.demo.course;

import com.example.demo.lesson.Lesson;
import com.example.demo.lesson.LessonDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/course")
@AllArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @GetMapping("/{id}")
    public Course getById(@PathVariable("id") Long id) {
        return courseService.getById(id);
    }

    @GetMapping
    public Iterable<Course> getAll() {
        return courseService.getAll();
    }

    @PostMapping
    public Course create(@Valid @RequestBody CourseDto courseDto) {
        return courseService.create(courseDto);
    }

    @PutMapping
    public void update(@Valid @RequestBody CourseDto courseDto) {
        courseService.update(courseDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        courseService.deleteById(id);
    }

    @DeleteMapping
    public void deleteAll() {
        courseService.deleteAll();
    }

    @PutMapping("/{id}/addLesson")
    public Lesson addLesson(@PathVariable("id") Long id, @Valid @RequestBody LessonDto lessonDto) {
        return courseService.addLesson(id, lessonDto);
    }

    @PostMapping("/createByCoping/{id}")
    public Course createByCoping(@PathVariable("id") Long id) {
        return courseService.createByCoping(id);
    }
}
