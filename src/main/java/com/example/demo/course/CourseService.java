package com.example.demo.course;

import com.example.demo.lecturer.Lecturer;
import com.example.demo.lesson.Lesson;
import com.example.demo.lesson.LessonDto;
import com.example.demo.user.UserPrincipal;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;

    @PostAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or returnObject.mentor.name == authentication.principal.username)")
    public Course getById(Long id) {
        return courseRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No course was found with id = " + id));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Iterable<Course> getAll() {
        return courseRepository.findAll();
    }

    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public Course create(CourseDto courseDto) {
        Course course = new Course(courseDto);

        Lecturer mentor = ((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getUser()
                .getLecturer();
        course.setMentor(mentor);

        return courseRepository.save(course);
    }

    @PostAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or returnObject.mentor.name == authentication.principal.username)")
    public Course update(CourseDto courseDto) {
        Course course = getById(courseDto.getId());

        course.setName(courseDto.getName());
        course.setDescription(courseDto.getDescription());
        course.setNumberOfStudents(courseDto.getNumberOfStudents());

        return courseRepository.save(course);
    }

    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or this.getById(#id).mentor.name == authentication.principal.username)")
    public void deleteById(Long id) {
        courseRepository.deleteById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteAll() {
        courseRepository.deleteAll();
    }

    @PreAuthorize("hasRole('ROLE_TEACHER')")
    public Course createByCoping(Long id) {
        Course course = getById(id);
        course.setLessons(new HashSet<>(createLessonsWithNullDate(course)));

        Lecturer mentor = ((UserPrincipal)SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getUser()
                .getLecturer();

        Course newCourse = new Course(mentor, course);

        return courseRepository.save(newCourse);
    }

    private Set<Lesson> createLessonsWithNullDate (Course course) {
        Set<Lesson> newLessons = new HashSet<>();
        for (Lesson lesson: course.getLessons()) {
            newLessons.add(new Lesson(lesson));
        }
        return newLessons;
    }

    @PostAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or this.getById(#courseId).mentor.name == authentication.principal.username)")
    public Lesson addLesson(Long courseId, LessonDto lessonDto) {
        if (!courseRepository.existsById(courseId)) {
            throw new EntityNotFoundException("No course was found to add lesson to with id = " + courseId);
        }
        Long lessonId = courseRepository.addLessonToCourse(courseId, lessonDto);
        return new Lesson(lessonId, lessonDto.getName(), lessonDto.getDescription(), lessonDto.getDate());
    }
}
