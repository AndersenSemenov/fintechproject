create table lecturer (
	id   bigserial primary key,
	name varchar(255) not null,
	age  integer not null
);

insert into lecturer(name, age) values('Alexandrov Pavel', 33);
insert into lecturer(name, age) values('Utkin Petr', 20);
insert into lecturer(name, age) values('Lebedev Yan', 28);

insert into lecturer(name, age) values('Fedkin Alex', 19);
insert into lecturer(name, age) values('Zaycev Andrey', 23)