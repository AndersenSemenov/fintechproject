create table course (
    id                 bigserial primary key,
    name               varchar(255) not null,
    description        varchar(255),
    number_of_students integer      not null,
    fk_category_id     bigint references category (id),
    fk_mentor_id       bigint references lecturer (id),
    fk_course_type     bigint references course_type (id)
);

insert into course(name, description, number_of_students, fk_category_id, fk_mentor_id, fk_course_type)
	values('Java course', 'Backend course', 38, 1, 1, 1);

insert into course(name, description, number_of_students, fk_category_id, fk_mentor_id, fk_course_type)
	values('Scala course', 'Another backend course', 13, 2, 2, 2);

insert into course(name, description, number_of_students, fk_category_id, fk_mentor_id, fk_course_type)
	values('History course', 'Discussion of Ancient Rome', 8, 3, 3, 2);