create table lesson (
    id             bigserial primary key,
    name           varchar(255) not null,
    description    varchar(255),
    fk_lecturer_id bigint references lecturer (id),
    fk_course_id   bigint references course (id),
    lesson_date    timestamp
);


insert into lesson(name, description, fk_lecturer_id, fk_course_id, lesson_date)
		values('First Java lesson', 'Maven intro', 1, 1, '2022-03-01 18:00:00-00');

insert into lesson(name, description, fk_lecturer_id, fk_course_id, lesson_date)
		values('Second Java lesson', 'Spring lesson', 4, 1, '2022-03-10 18:00:00-00');

insert into lesson(name, description, fk_lecturer_id, fk_course_id, lesson_date)
		values('Third Java lesson', 'Hibernate topic', 1, 1, '2022-03-16 18:00:00-00');


insert into lesson(name, description, fk_lecturer_id, fk_course_id, lesson_date)
		values('First Scala lesson', 'JVM', 2, 2, '2022-03-04 18:00:00-00');

insert into lesson(name, description, fk_lecturer_id, fk_course_id, lesson_date)
		values('Second Scala lesson', 'Scala base library', 5, 2, '2022-03-11 18:00:00-00');

insert into lesson(name, description, fk_lecturer_id, fk_course_id, lesson_date)
		values('Third Scala lesson', 'Functional programming on Scala', 5, 2, '2022-03-18 18:00:00-00');


insert into lesson(name, description, fk_lecturer_id, fk_course_id, lesson_date)
		values('First discussion meeting', 'Beginning of Ancient Rome', 3, 3, '2022-03-12 18:00:00-00');

insert into lesson(name, description, fk_lecturer_id, fk_course_id, lesson_date)
		values('Second discussion meeting', 'Ancient Rome: latest', 3, 3, '2022-03-28 18:00:00-00');