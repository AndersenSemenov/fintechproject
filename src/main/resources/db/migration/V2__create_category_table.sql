create table category (
    id bigserial primary key
);

create table technical_category (
    id       bigserial primary key references category (id),
    language varchar(255) not null
);

create table humanitarian_category (
    id                bigserial primary key references category (id),
    number_of_debates integer not null
);

insert into category(id) values(default);
insert into technical_category(id, language) values(1, 'Java');

insert into category(id) values(default);
insert into technical_category(id, language) values(2, 'Scala');

insert into category(id) values(default);
insert into humanitarian_category(id, number_of_debates) values(3, 5);
