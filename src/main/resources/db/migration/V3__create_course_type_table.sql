create table course_type (
    id bigserial primary key
);

create table online_course (
    id  bigserial primary key references course_type (id),
    url varchar(255) not null
);

create table offline_course (
    id              bigserial primary key references course_type (id),
    university_name varchar(255) not null,
    address         varchar(255) not null
);


insert into course_type(id) values(default);
insert into online_course(id, url) values(1, 'zoom.staff');

insert into course_type(id) values(default);
insert into offline_course(id, university_name, address) values(2, 'SPbU', 'Universitetskaya ul., 28');