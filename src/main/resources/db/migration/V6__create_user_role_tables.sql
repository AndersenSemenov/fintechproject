create table if not exists role (
	id   bigserial primary key,
	name varchar(255) not null
);

create table if not exists users (
	id            bigserial primary key,
	name          varchar(255) not null, --make unique
	password      varchar(255) not null,
	fk_role_id    bigint references role(id),
	fk_lecturer_id bigint unique references lecturer(id)
);

insert into role(name) values('ROLE_ADMIN');
insert into role(name) values('ROLE_TEACHER');

insert into users(name, password, fk_role_id) values('admin', '$2a$12$Ac4Bj0U5UFNAawmU.4D7iOONlyT9.4ZVi8.TyDM8Puza5JZRKfbYm', 1);
insert into users(name, password, fk_role_id, fk_lecturer_id) values('Alexandrov Pavel', '$2a$12$yiBTj4EAY3Vi/zAek9FLk.h0DMl8iArHAXh0S4X/e7H9wMBk2tRTC', 2, 1);
insert into users(name, password, fk_role_id, fk_lecturer_id) values('Utkin Petr', '$2a$12$APTfRydDXCDElCRd/nna2..jjzoT4ooAmlJTgkJym0nEISXNppoqm', 2, 2);
insert into users(name, password, fk_role_id, fk_lecturer_id) values('Lebedev Yan', '$2a$12$qdOYDc9WEHQTqKTqu47wGOXZahj693Pbm61EoL5i8Hf5QICLcgBli', 2, 3);
insert into users(name, password, fk_role_id, fk_lecturer_id) values('Fedkin Alex', '$2a$12$3jAf3BONSSkiY6Mp1QrN1.CjPpShACK8TgxckhgWinnY3Pu7ngtLW', 2, 4);
insert into users(name, password, fk_role_id, fk_lecturer_id) values('Zaycev Andrey', '$2a$12$3/bJD.1rfVNzNYDSbb9OPO1icti3OAJSRD4k3.9aU2a7n/mqqSd8K', 2, 5);


