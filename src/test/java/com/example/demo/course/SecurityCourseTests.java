package com.example.demo.course;

import com.example.demo.IntegrationTests;
import com.example.demo.category.abstract_category.Category;
import com.example.demo.category.technical_category.TechnicalCategory;
import com.example.demo.course_type.abstract_course_type.CourseType;
import com.example.demo.course_type.online_course.OnlineCourse;
import com.example.demo.lecturer.Lecturer;
import com.example.demo.lesson.LessonDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.HashSet;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class SecurityCourseTests extends IntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CourseRepository courseRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToGetNotYourCourse() throws Exception {
        mockMvc.perform(get("/course/{id}", 1L))
                .andExpect(status().isForbidden());
    }


    @Test
    public void tryToGetCourseUnauthorised() throws Exception {
        mockMvc.perform(get("/course/{id}", 1L))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToGetAllCourses() throws Exception {
        mockMvc.perform(get("/course"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void tryToCreate() throws Exception {
        Course course = new Course(4L, "new", "..", 4, new HashSet<>(), new Lecturer(), new TechnicalCategory(), new OnlineCourse());
        String content = objectMapper.writeValueAsString(course);
        mockMvc.perform(post("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("Utkin Petr")
    public void tryToUpdate() throws Exception {
        Category category = new TechnicalCategory(1L, "Java");
        CourseType courseType = new OnlineCourse(1L, "zoom.staff");
        CourseDto course = new CourseDto(1L, "Java 2.0 course", "New backend course", 50, category, courseType);

        String content = objectMapper.writeValueAsString(course);

        mockMvc.perform(put("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToDeleteById() throws Exception {
        mockMvc.perform(delete("/course/{id}", 1L))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToDeleteAll() throws Exception {
        mockMvc.perform(delete("/course"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void tryToCreateByCoping() throws Exception {
        mockMvc.perform(post("/course/createByCoping/{id}", 1L))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToAddLessonNotToYourCourse() throws Exception {
        LessonDto lessonDto = new LessonDto(null, "Another java lesson", "GC discussion", new Date());

        String content = objectMapper.writeValueAsString(lessonDto);

        mockMvc.perform(put("/course/{courseId}/addLesson", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}
