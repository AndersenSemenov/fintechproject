package com.example.demo.course;

import com.example.demo.IntegrationTests;
import com.example.demo.category.abstract_category.Category;
import com.example.demo.category.humanitarian_category.HumanitarianCategory;
import com.example.demo.category.technical_category.TechnicalCategory;
import com.example.demo.course_type.abstract_course_type.CourseType;
import com.example.demo.course_type.offline_course.OfflineCourse;
import com.example.demo.course_type.online_course.OnlineCourse;
import com.example.demo.lecturer.Lecturer;
import com.example.demo.lecturer.LecturerRepository;
import com.example.demo.lesson.Lesson;
import com.example.demo.lesson.LessonDto;
import com.example.demo.lesson.LessonRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.persistence.EntityNotFoundException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@AutoConfigureMockMvc
public class CourseControllerTests extends IntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LecturerRepository lecturerRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void getById() throws Exception {
        Long id = 1L;
        Category category = new TechnicalCategory(1L, "Java");
        CourseType courseType = new OnlineCourse(1L, "zoom.staff");
        CourseDto course = new CourseDto(id, "Java course", "Backend course", 38, category, courseType);

        String expectedContent = objectMapper.writeValueAsString(course);

        mockMvc.perform(get("/course/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @WithUserDetails("admin")
    public void getByNonExistingId() throws Exception {
        mockMvc.perform(get("/course/{id}", 42))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No course was found with id = 42", result.getResolvedException().getMessage()));
    }

    @Test
    @WithUserDetails("admin")
    public void getAll() throws Exception {
        Category javaCategory = new TechnicalCategory(1L, "Java");
        Category scalaCategory = new TechnicalCategory(2L, "Scala");
        Category humanCategory = new HumanitarianCategory(3L, 5);

        CourseType onlineCourse = new OnlineCourse(1L, "zoom.staff");
        CourseType offlineCourse = new OfflineCourse(2L, "SPbU", "Universitetskaya ul., 28");

        CourseDto course1 = new CourseDto(1L, "Java course", "Backend course", 38, javaCategory, onlineCourse);
        CourseDto course2 = new CourseDto(2L, "Scala course", "Another backend course", 13, scalaCategory, offlineCourse);
        CourseDto course3 = new CourseDto(3L, "History course", "Discussion of Ancient Rome", 8, humanCategory, offlineCourse);

        String expectedContent = objectMapper.writeValueAsString(List.of(course1, course2, course3));

        mockMvc.perform(get("/course"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    private boolean areEqual(Course expected, Course actual) {
        return expected.getName().equals(actual.getName())
                && expected.getDescription().equals(actual.getDescription())
                && expected.getNumberOfStudents().equals(actual.getNumberOfStudents())
                && expected.getMentor().equals(actual.getMentor())
                && expected.getLessons().equals(actual.getLessons()) //проблема на create
                && expected.getCategory().equals(actual.getCategory())
                && expected.getCourseType().equals(actual.getCourseType());
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void create() throws Exception {
        Category category = new HumanitarianCategory(3L, 5);
        CourseType courseType = new OnlineCourse(1L, "zoom.staff");
        Lecturer mentor = lecturerRepository.findById(1L).get();
        CourseDto courseDto = new CourseDto(null, "new", "..", 4, category, courseType);
        Course expectedCourse = new Course(4L, "new", "..", 4, new HashSet<>(), mentor, category, courseType);

        String content = objectMapper.writeValueAsString(courseDto);

        mockMvc.perform(post("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertTrue(areEqual(expectedCourse, courseRepository.findById(4L).get())))
                .andExpect(x -> assertEquals(2, lecturerRepository.findById(1L).get().getMentoredCourses().size()));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void createWithValidationError() throws Exception {
        CourseDto courseDto = new CourseDto(null, "new", "..", -4, new TechnicalCategory(), new OnlineCourse());

        String content = objectMapper.writeValueAsString(courseDto);

        mockMvc.perform(post("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.numberOfStudents", Is.is("Number of students can't be less than 0")));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void update() throws Exception {
        Category category = new TechnicalCategory(1L, "Java");
        CourseType courseType = new OnlineCourse(1L, "zoom.staff");
        CourseDto course = new CourseDto(1L, "Java 2.0 course", "New backend course", 50, category, courseType);

        String content = objectMapper.writeValueAsString(course);

        mockMvc.perform(put("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(course, courseRepository.findById(1L).get().toDto()));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void updateWithValidationError() throws Exception {
        CourseDto course = new CourseDto(1L, "", "New backend course", 50, new TechnicalCategory(), new OnlineCourse());

        String content = objectMapper.writeValueAsString(course);

        mockMvc.perform(put("/course")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", Is.is("Course name can't be empty")));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void deleteById() throws Exception {
        mockMvc.perform(delete("/course/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(courseRepository.findById(1L).isPresent()))
                .andExpect(x -> assertEquals(0, lecturerRepository.findById(1L).get().getMentoredCourses().size()));
    }

    @Test
    @WithUserDetails("admin")
    public void deleteAll() throws Exception {
        mockMvc.perform(delete("/course"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void addLesson() throws Exception {
        LessonDto lessonDto = new LessonDto(null, "Another java lesson", "GC discussion", new Date());

        Lesson expectedLesson = new Lesson(9L, "Another java lesson", "GC discussion", new Date());

        String content = objectMapper.writeValueAsString(lessonDto);

        mockMvc.perform(put("/course/{courseId}/addLesson", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedLesson, lessonRepository.findById(9L).get()))
                .andExpect(x -> assertEquals(4, courseRepository.findById(1L).get().getLessons().size()));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void addLessonToNonExistingCourse() throws Exception {
        LessonDto lessonDto = new LessonDto(null, "Another java lesson", "GC discussion", new Date());

        String content = objectMapper.writeValueAsString(lessonDto);

        mockMvc.perform(put("/course/{courseId}/addLesson", 67L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No course was found to add lesson to with id = 67", result.getResolvedException().getMessage()));
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void createByCoping() throws Exception {
        Lecturer mentor = lecturerRepository.findById(3L).get();

        Set<Lesson> lessons = new HashSet<>();
        lessons.add(new Lesson(1L, "First Java lesson", "Maven intro", null));
        lessons.add(new Lesson(2L, "Second Java lesson", "Spring lesson", null));
        lessons.add(new Lesson(3L, "Third Java lesson", "Hibernate topic", null));

        Category category = new TechnicalCategory(1L, "Java");
        CourseType courseType = new OnlineCourse(1L, "zoom.staff");
        Course expectedCourse = new Course(4L, "Java course", "Backend course", 38, new HashSet<>(), mentor, category, courseType);

        mockMvc.perform(post("/course/createByCoping/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(x -> assertTrue(areEqual(expectedCourse, courseRepository.findById(4L).get())))
                .andExpect(x -> assertEquals(2, lecturerRepository.findById(3L).get().getMentoredCourses().size()));
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void createByCopingNonExistingCourse() throws Exception {
        mockMvc.perform(post("/course/createByCoping/{id}", 11L))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No course was found with id = 11", result.getResolvedException().getMessage()));
    }
}