package com.example.demo.course;

import com.example.demo.IntegrationTests;
import com.example.demo.lesson.LessonDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CourseRepositoryTests extends IntegrationTests {

    @Autowired
    private CourseRepository courseRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void addLessonToCourse() {
        LessonDto lessonDto = new LessonDto(null, "Another java lesson", "GC discussion", new Date());
        courseRepository.addLessonToCourse(1L, lessonDto);
        assertEquals(4, courseRepository.findById(1L).get().getLessons().size());
    }
}
