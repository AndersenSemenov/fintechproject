package com.example.demo.course_type.abstract_course_type;

import com.example.demo.IntegrationTests;
import com.example.demo.course_type.offline_course.OfflineCourse;
import com.example.demo.course_type.offline_course.OfflineCourseRepository;
import com.example.demo.course_type.online_course.OnlineCourse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.persistence.EntityNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@WithUserDetails("Lebedev Yan")
public class CourseTypeControllerTests extends IntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private OfflineCourseRepository offlineRepo;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void getById() throws Exception {
        CourseType onlineCourse = new OnlineCourse(1L, "zoom.staff");

        String content = objectMapper.writeValueAsString(onlineCourse);

        mockMvc.perform(get("/typecourse/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().string(content));
    }

    @Test
    public void getByNonExistingId() throws Exception {
        mockMvc.perform(get("/typecourse/{id}", 1190L))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No course type was found with id = 1190", result.getResolvedException().getMessage()));
    }

    @Test
    public void create() throws Exception {
        OfflineCourse offlineCourse = new OfflineCourse(3L, "SPbU", "...()...");

        String content = objectMapper.writeValueAsString(offlineCourse);

        mockMvc.perform(post("/typecourse")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(offlineCourse, offlineRepo.findById(3L).get()));
    }

    @Test
    public void createWithValidationError() throws Exception {
        CourseType onlineCourse = new OnlineCourse(null, "");

        String content = objectMapper.writeValueAsString(onlineCourse);

        mockMvc.perform(post("/typecourse")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.url", Is.is("url for course can't be empty")));
    }
}
