package com.example.demo.category.abstract_category;

import com.example.demo.IntegrationTests;
import com.example.demo.category.technical_category.TechnicalCategory;
import com.example.demo.category.technical_category.TechnicalRepository;
import com.example.demo.lesson.LessonRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.persistence.EntityNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@WithUserDetails("Alexandrov Pavel")
public class CategoryControllerTests extends IntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TechnicalRepository techRepo;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void getById() throws Exception {
        TechnicalCategory technicalCategory = new TechnicalCategory(1L, "Java");

        String content = objectMapper.writeValueAsString(technicalCategory);

        mockMvc.perform(get("/category/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().string(content));
    }

    @Test
    public void getByNonExistingId() throws Exception {
        mockMvc.perform(get("/category/{id}", 33L))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No category was found with id = 33", result.getResolvedException().getMessage()));
    }

    @Test
    public void create() throws Exception {
        Category category = new TechnicalCategory(4L, "Kotlin");

        String content = objectMapper.writeValueAsString(category);

        mockMvc.perform(post("/category")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(category, techRepo.findById(4L).get()));
    }

    @Test
    public void createWithValidationError() throws Exception {
        Category category = new TechnicalCategory(null, "");

        String content = objectMapper.writeValueAsString(category);

        mockMvc.perform(post("/category")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.language", Is.is("Language name can't be empty")));
    }
}
