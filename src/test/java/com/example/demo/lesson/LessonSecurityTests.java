package com.example.demo.lesson;

import com.example.demo.IntegrationTests;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.GregorianCalendar;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class LessonSecurityTests extends IntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void tryToGet() throws Exception {
        mockMvc.perform(get("/lesson/{id}", 7))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToUpdate() throws Exception {
        GregorianCalendar calendar = new GregorianCalendar(2022, 3, 2, 18, 0, 0);
        LessonDto lessonDto = new LessonDto(1L, "changed lesson", "new descr", calendar.getTime());

        String content = objectMapper.writeValueAsString(lessonDto);

        mockMvc.perform(put("/lesson")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToDelete() throws Exception {
        mockMvc.perform(delete("/lesson/{id}", 2L))
                .andExpect(status().isForbidden());
    }
}
