package com.example.demo.lesson;

import com.example.demo.IntegrationTests;
import com.example.demo.lecturer.LecturerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.testcontainers.shaded.com.trilead.ssh2.packets.PacketUserauthBanner;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
public class LessonControllerTests extends IntegrationTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LecturerRepository lecturerRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void getById() throws Exception {
        GregorianCalendar calendar = new GregorianCalendar(2022, 2, 1, 18, 0, 0);
        Lesson lesson = new Lesson(1L, "First Java lesson", "Maven intro", calendar.getTime());
        String content = objectMapper.writeValueAsString(lesson);
        mockMvc.perform(get("/lesson/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().string(content));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void getByNonExistingId() throws Exception {
        mockMvc.perform(get("/lesson/{id}", 351L))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No lesson was found with id = 351", result.getResolvedException().getMessage()));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void update() throws Exception {
        GregorianCalendar calendar = new GregorianCalendar(2022, 3, 2, 18, 0, 0);
        LessonDto lessonDto = new LessonDto(1L, "changed lesson", "new descr", calendar.getTime());

        String content = objectMapper.writeValueAsString(lessonDto);

        mockMvc.perform(put("/lesson")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(lessonDto, lessonRepository.findById(1L).get().toDto()));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void updateWithValidationError() throws Exception {
        LessonDto lessonDto = new LessonDto(1L, "", "new descr", new Date());

        String content = objectMapper.writeValueAsString(lessonDto);

        mockMvc.perform(put("/lesson")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.name", Is.is("Lesson name can't be empty")));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void deleteById() throws Exception {
        mockMvc.perform(delete("/lesson/{id}", 2L))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(lessonRepository.findById(2L).isPresent()))
                .andExpect(x -> assertEquals(0, lecturerRepository.findById(4L).get().getLessons().size()));
    }
}
