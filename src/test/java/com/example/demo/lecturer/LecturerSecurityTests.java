package com.example.demo.lecturer;

import com.example.demo.IntegrationTests;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class LecturerSecurityTests extends IntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void tryToGetAllLecturers() throws Exception {
        mockMvc.perform(get("/lecturer"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void tryToGetAllLecturersUnauthorized() throws Exception {
        mockMvc.perform(get("/lecturer"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void tryToGetLecturer() throws Exception {
        mockMvc.perform(get("/lecturer/{id}", 4L))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToGetSchedule() throws Exception {
        mockMvc.perform(get("/lecturer/schedule/{id}", 4L))
                .andExpect(status().isForbidden());
    }

    @Test
    public void tryToGetScheduleUnauthorized() throws Exception {
        mockMvc.perform(get("/lecturer/schedule/{id}", 4L))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToGetMentoredCourses() throws Exception {
        mockMvc.perform(get("/lecturer/schedule/{id}", 4L))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToUpdate() throws Exception {
        LecturerDto lecturerDto = new LecturerDto(1L, "Not Alex", 10, "new");

        String content = objectMapper.writeValueAsString(lecturerDto);

        mockMvc.perform(put("/lecturer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Fedkin Alex")
    public void tryToDeleteAnotherLecturer() throws Exception {
        mockMvc.perform(delete("/lecturer/{id}", 1L))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Fedkin Alex")
    public void tryToDeleteAllLecturers() throws Exception {
        mockMvc.perform(delete("/lecturer"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToLeaveCourseMentoringByAnotherMentor() throws Exception {
        mockMvc.perform(put("/lecturer/{lecturerId}/stopMentoring/{courseId}", 1L, 1L))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToGetCourseMentoring() throws Exception {
        mockMvc.perform(put("/lecturer/{lecturerId}/getMentoring/{courseId}", 2L, 1L))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToLeaveLessonByAnotherLecturer() throws Exception {
        mockMvc.perform(put("/lecturer/{lecturerId}/leaveLesson/{lessonId}", 1L, 1L))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("Lebedev Yan")
    public void tryToGetLesson() throws Exception {
        mockMvc.perform(put("/lecturer/{lecturerId}/getLesson/{lessonId}", 3L, 1L))
                .andExpect(status().isForbidden());
    }
}
