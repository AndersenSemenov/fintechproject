package com.example.demo.lecturer;

import com.example.demo.IntegrationTests;
import com.example.demo.course.CourseRepository;
import com.example.demo.lesson.LessonRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LecturerRepositoryTests extends IntegrationTests {

    @Autowired
    private LecturerRepository lecturerRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    public void leaveCourseMentoring() {
        lecturerRepository.leaveCourseMentoring(1L, 1L);
        assertEquals(null, courseRepository.findById(1L).get().getMentor());
    }

    @Test
    public void getCourseMentoring() {
        lecturerRepository.getCourseMentoring(2L, 1L);
        assertEquals(2L, courseRepository.findById(1L).get().getMentor().getId());
    }

    @Test
    public void leaveLesson() {
        lecturerRepository.leaveLesson(1L, 1L);
        assertEquals(null, lessonRepository.findById(1L).get().getLecturer());
    }

    @Test
    public void getLesson() {
        lecturerRepository.getLesson(3L, 1L);
        assertEquals(3L, lessonRepository.findById(1L).get().getLecturer().getId());
    }
}
