package com.example.demo.lecturer;

import com.example.demo.IntegrationTests;
import com.example.demo.course.CourseRepository;
import com.example.demo.lesson.Lesson;
import com.example.demo.lesson.LessonRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.persistence.EntityNotFoundException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
public class LecturerControllerTests extends IntegrationTests {

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private LecturerRepository lecturerRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithUserDetails("admin")
    public void getAll() throws Exception {
        Lecturer lecturer1 = new Lecturer(1L, "Alexandrov Pavel", 33);
        Lecturer lecturer2 = new Lecturer(2L, "Utkin Petr", 20);
        Lecturer lecturer3 = new Lecturer(3L, "Lebedev Yan", 28);
        Lecturer lecturer4 = new Lecturer(4L, "Fedkin Alex", 19);
        Lecturer lecturer5 = new Lecturer(5L, "Zaycev Andrey", 23);

        List<Lecturer> lecturers = List.of(lecturer1, lecturer2, lecturer3, lecturer4, lecturer5);

        String expectedContent = objectMapper.writeValueAsString(lecturers);

        mockMvc.perform(get("/lecturer"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @WithUserDetails("admin")
    public void getById() throws Exception {
        Lecturer lecturer = new Lecturer(1L, "Alexandrov Pavel", 33);

        String content = objectMapper.writeValueAsString(lecturer);

        mockMvc.perform(get("/lecturer/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().string(content));
    }

    @Test
    @WithUserDetails("admin")
    public void getByNonExistingId() throws Exception {
        mockMvc.perform(get("/lecturer/{id}", 42L))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof EntityNotFoundException))
                .andExpect(result -> assertEquals("No lecturer was found with id = 42", result.getResolvedException().getMessage()));
    }

    @Test
    public void create() throws Exception {
        LecturerDto lecturerDto = new LecturerDto(null, "lect", 22, "wqe");
        Lecturer expectedLecturer = new Lecturer(6L, "lect", 22);

        String content = objectMapper.writeValueAsString(lecturerDto);

        mockMvc.perform(post("/lecturer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(expectedLecturer, lecturerRepository.findById(6L).get()));
    }

    @Test
    public void createWithValidationError() throws Exception {
        LecturerDto lecturerDto = new LecturerDto(null, "lect", -10, "wqe");

        String content = objectMapper.writeValueAsString(lecturerDto);

        mockMvc.perform(post("/lecturer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.age", Is.is("Age can't be less than 0")));
    }

    @Test
    @WithUserDetails("admin")
    public void getSchedule() throws Exception {
        Date date1 = new GregorianCalendar(2022, Calendar.MARCH, 1, 18, 0, 0).getTime();
        Lesson lesson1 = new Lesson(1L, "First Java lesson", "Maven intro", date1);

        Date date2 = new GregorianCalendar(2022, Calendar.MARCH, 16, 18, 0, 0).getTime();
        Lesson lesson2 = new Lesson(3L, "Third Java lesson", "Hibernate topic", date2);

        String expectedContent = objectMapper.writeValueAsString(List.of(lesson1, lesson2));

        mockMvc.perform(get("/lecturer/schedule/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void update() throws Exception {
        LecturerDto lecturerDto = new LecturerDto(1L, "Not Alex", 10, "new");

        String content = objectMapper.writeValueAsString(lecturerDto);

        mockMvc.perform(put("/lecturer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(lecturerDto, lecturerRepository.findById(1L).get().toDto()))
                .andExpect(x -> encoder.matches("new", lecturerRepository.findById(1L).get().getUser().getPassword()));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void updateWithValidationError() throws Exception {
        LecturerDto lecturerDto = new LecturerDto(1L, "Alexandrov Pavel", -12, "new");

        String content = objectMapper.writeValueAsString(lecturerDto);

        mockMvc.perform(put("/lecturer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException))
                .andExpect(jsonPath("$.age", Is.is("Age can't be less than 0")));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void deleteById() throws Exception {
        mockMvc.perform(delete("/lecturer/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(x -> assertFalse(lecturerRepository.findById(1L).isPresent()))
                .andExpect(x -> assertNull(courseRepository.findById(1L).get().getMentor()))
                .andExpect(x -> assertNull(lessonRepository.findById(1L).get().getLecturer()));
    }

    @Test
    @WithUserDetails("admin")
    public void deleteAll() throws Exception {
        mockMvc.perform(delete("/lecturer"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void leaveCourseMentoring() throws Exception {
        mockMvc.perform(put("/lecturer/{lecturerId}/stopMentoring/{courseId}", 1L, 1L))
                .andExpect(status().isOk())
                .andExpect(x -> assertNull(courseRepository.findById(1L).get().getMentor()));
    }

    @Test
    @WithUserDetails("admin")
    public void getCourseMentoring() throws Exception {
        mockMvc.perform(put("/lecturer/{lecturerId}/getMentoring/{courseId}", 3L, 1L))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(3, courseRepository.findById(1L).get().getMentor().getId()));
    }

    @Test
    @WithUserDetails("Alexandrov Pavel")
    public void leaveLesson() throws Exception {
        mockMvc.perform(put("/lecturer/{lecturerId}/leaveLesson/{lessonId}", 1L, 1L))
                .andExpect(status().isOk())
                .andExpect(x -> assertNull(lessonRepository.findById(1L).get().getLecturer()));
    }

    @Test
    @WithUserDetails("admin")
    public void getLesson() throws Exception {
        mockMvc.perform(put("/lecturer/{lecturerId}/getLesson/{lessonId}", 2L, 1L))
                .andExpect(status().isOk())
                .andExpect(x -> assertEquals(2, (long) lessonRepository.findById(1L).get().getLecturer().getId()));
    }
}
